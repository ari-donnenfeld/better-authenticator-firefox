document.addEventListener('DOMContentLoaded', function() {

    // Letter Picker
    const source = document.getElementById('letterinput');
    const result = document.getElementById('letteroutput');
    const inputHandler = function(e) {
        result.innerText = e.target.value.substring(0,1).toUpperCase();
    }
    source.addEventListener('input', inputHandler);
    source.addEventListener('propertychange', inputHandler); // for IE8

    /* 
     * Note, Color picker doesnt work on firefox (the extention loses focus), so I've had to include an embedded one
    */

    const picker = new CP(document.getElementById('colorpicker'));

    // Allows preview by change of picker
    picker.on('change', function(r, g, b, a) {

        // Set textinput box and preview background
        document.getElementById("colorpicker").value = this.color(r, g, b, 1);
        document.getElementById("example_div").style.backgroundColor = this.color(r, g, b, 1);

        // Change text color based on background
        const brightness = Math.round(((parseInt(r) * 299) + (parseInt(g) * 587) + (parseInt(b) * 114)) / 1000);
        if (brightness < 60) {
            result.classList.add("bright");
        } else {
            result.classList.remove("bright");
        }
    });
    
    // Account Selector Setup
    chrome.storage.local.get(["computerList"], function(items){
        try {
            let list = items["computerList"];
            for (var key in list) {
                extras_select = document.getElementById("extras")
                var opt = document.createElement('option');
                opt.value = list[key].issuer;
                opt.innerText = list[key].issuer;
                extras_select.appendChild(opt);
            }
        } catch (e) {
            console.log("THERES AN ERROR. ID: ES1");
        }
    });

    // Confirm Button
    var confirm_button = document.getElementById('confirm_button');
    confirm_button.addEventListener('click', function() {
        chrome.storage.local.get(["extras"], function(items){
            items = items["extras"]
            // Setup items as list if string
            if (!items || typeof(items) != typeof([])) {items = [];}
            // Append item to list of items
            items.push([document.getElementById("colorpicker").value, document.getElementById("letterinput").value.substring(0,1).toUpperCase(), document.getElementById("extras").value])
            // Push Items to storage
            chrome.storage.local.set({ "extras": items}, function(){});

            window.location.href = '/post.html';
        })
    }, false);

}, false);
